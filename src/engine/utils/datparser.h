#pragma once

#include <fstream>
#include <unordered_map>

namespace utils {

	constexpr int type_int = 1;
	constexpr int type_bool = 2;
	constexpr int type_str = 3;
	constexpr int type_cont = 4;

	struct container {
		std::unordered_map<std::string, std::string> data;
	};

	container loadcontainerfromfile(std::string path);
	void printcontainer(container cont);
	std::string parse_str(std::string val);
	int parse_num(std::string val);
	bool parse_bool(std::string val);

}