#include <engine/utils/datparser.h>
#include <iostream>

namespace utils {

	std::ifstream _instream;

	std::string trim(const std::string& str, const std::string& whitespace = " \t") {
	    const auto strBegin = str.find_first_not_of(whitespace);
	    if (strBegin == std::string::npos)
	        return ""; // no content

	    const auto strEnd = str.find_last_not_of(whitespace);
	    const auto strRange = strEnd - strBegin + 1;

	    return str.substr(strBegin, strRange);
	}

	container loadcontainerfromfile(std::string path) {
		_instream.open(path);

		container _cont;

		std::string line;

		while(std::getline(_instream, line)) {
			if (line.size() > 0) {
				size_t pos = line.find_first_of(":");
				
				if(pos > line.size()) break;
				std::string name = trim(line.substr(0, pos));
				std::string data = trim(line.substr(pos + 1, line.size()));

				_cont.data[name] = data;

			}
		}

		return _cont;
	}


	void printcontainer(container cont) {
		for (const auto& i : cont.data) {
			std::cout << i.first << ": " << i.second << "\n";
		}
	}

	std::string parse_str(std::string val) {
		return val.substr(1, val.size() - 1);
	}

	int parse_num(std::string val) {
		return std::stoi(val);
	}

	bool parse_bool(std::string val) {
		return val == "true";
	}

}