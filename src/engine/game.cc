#include <engine/game.h>

namespace engine {

    using states::state_ptr;

	game::game() :_window(sf::VideoMode(854, 480), "Ludum Dare 40"), _statemanager(), _inputmanager(_statemanager) {
        _window.setFramerateLimit(60);
        _statemanager.add_state("game",  state_ptr(new states::gamestate()));
        _statemanager.enter_state("game");
	}

	game::~game() {
		close();
	}

    void game::close(void) {
        _window.close();
    }

    bool should_close = false;

    using namespace std::chrono_literals;

	void game::run() {
		constexpr std::chrono::nanoseconds timestep(16ms);
    
        using clock = std::chrono::high_resolution_clock;
    
        std::chrono::nanoseconds lag(0ns);
        auto time_start = clock::now();
    
        while (_window.isOpen()) {
            auto delta_time = clock::now() - time_start;
            time_start = clock::now();
            lag += std::chrono::duration_cast<std::chrono::nanoseconds>(delta_time);
    
            sf::Event event;
            pollEvents(event);
            if (should_close) return;

            while (lag >= timestep) {
                lag -= timestep;
                _inputmanager.update();
                _statemanager.get_current_state()->update();
                //update();
            }

            render();
        }
	}

    void game::pollEvents(sf::Event& event) {
        while (_window.pollEvent(event)) {
            if (event.type == sf::Event::Closed) should_close = true;
        }
    }

    void game::render() {
        _statemanager.get_current_state()->render(_window);
        _window.display();
    }

}