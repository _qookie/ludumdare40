#pragma once

#include <stdint.h>
#include <SFML/Graphics.hpp>

namespace entity {

	class entity : public sf::Drawable {
	public:		
		virtual int getX(void) const = 0;
		virtual int getY(void) const = 0;
		virtual void setX(int) = 0;
		virtual void setY(int) = 0;
		virtual void update(void) = 0;
		virtual void input(uint16_t) = 0;
		
	private:
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const = 0;
	};

}