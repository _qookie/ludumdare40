#include <engine/entity/maprenderer.h>

namespace entity {

	maprenderer::maprenderer(std::array<int, 256 * 256>& tiles, std::array<int, 256 * 256>& items) :_tiles(tiles), _items(items) {
		recreatearray();
	}

	maprenderer::~maprenderer() {

	}

	int maprenderer::getX(void) const {
		return _x;
	}

	int maprenderer::getY(void) const {
		return _y;
	}

	void maprenderer::setX(int x) {
		_x = x;
		//recreatearray();
	}

	void maprenderer::setY(int y) {
		_y = y;
		//recreatearray();
	}

	void maprenderer::update(void) {

	}

	void maprenderer::input(uint16_t code) {
		(void)code;
	}
	
	void maprenderer::draw(sf::RenderTarget& target, sf::RenderStates states) const {
		target.draw(_array, states);
	}

	void maprenderer::recreatearray() {
        
        _array = sf::VertexArray(sf::Quads);

        for (int y = 0; y < 256; ++y) {
            for (int x = 0; x < 256; ++x) {

                sf::Vertex vertex1;
                sf::Vertex vertex2;
                sf::Vertex vertex3;
                sf::Vertex vertex4;

                sf::Vertex ivertex1;
                sf::Vertex ivertex2;
                sf::Vertex ivertex3;
                sf::Vertex ivertex4;


                int l = _items[x + 256 * y];
                int k = _tiles[x + 256 * y];

                int u = 32 * (k % 8);
                int v = 32 * (k / 8);
                int u2 = u + 32;
                int v2 = v + 32;
                float xx = _x + (x * 32);
                float yy = _y + (y * 32);
                float xx2 = xx + 32;
                float yy2 = yy + 32;

                vertex1.position.x = xx;
                vertex1.position.y = yy;
                vertex1.texCoords.x = u;
                vertex1.texCoords.y = v;
                _array.append(vertex1);

                vertex2.position.x = xx;
                vertex2.position.y = yy2;
                vertex2.texCoords.x = u;
                vertex2.texCoords.y = v2;
                _array.append(vertex2);

                vertex3.position.x = xx2;
                vertex3.position.y = yy2;
                vertex3.texCoords.x = u2;
                vertex3.texCoords.y = v2;
                _array.append(vertex3);

                vertex4.position.x = xx2;
                vertex4.position.y = yy;
                vertex4.texCoords.x = u2;
                vertex4.texCoords.y = v;
                _array.append(vertex4);

                if (l > 0) {
                    int uu = 32 * (l % 8);
                    int vv = 32 * (l / 8);
                    int uu2 = uu + 32;
                    int vv2 = vv + 32;
                    float xxx = _x + (x * 32);
                    float yyy = _y + (y * 32);
                    float xxx2 = xx + 32;
                    float yyy2 = yy + 32;
                    
                    ivertex1.position.x = xxx;
                    ivertex1.position.y = yyy;
                    ivertex1.texCoords.x = uu;
                    ivertex1.texCoords.y = vv;
                    _array.append(ivertex1);

                    ivertex2.position.x = xxx;
                    ivertex2.position.y = yyy2;
                    ivertex2.texCoords.x = uu;
                    ivertex2.texCoords.y = vv2;
                    _array.append(ivertex2);

                    ivertex3.position.x = xxx2;
                    ivertex3.position.y = yyy2;
                    ivertex3.texCoords.x = uu2;
                    ivertex3.texCoords.y = vv2;
                    _array.append(ivertex3);

                    ivertex4.position.x = xxx2;
                    ivertex4.position.y = yyy;
                    ivertex4.texCoords.x = uu2;
                    ivertex4.texCoords.y = vv;
                    _array.append(ivertex4);

                }

            }
        }
    }

}