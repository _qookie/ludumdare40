#include <engine/entity/zombie.h>
#include <iostream>
#include <ctime>

namespace entity {

	zombie::zombie(std::array<int,256 * 256>& map, int x, int y, int& px, int& py) :_sprite(), _map(map), _px(px), _py(py) {
		_sprite.setPosition(32, 32);
		_tex.loadFromFile("res/zombie.png", sf::IntRect{0,0,32,32});
		_sprite.setTexture(_tex, true);
		srand((unsigned)time(NULL));
		_sprite.setPosition(x, y);
	}

	zombie::~zombie() {

	}

	int zombie::getX(void) const {
		return _sprite.getPosition().x;
	}

	int zombie::getY(void) const {
		return _sprite.getPosition().y;
	}

	void zombie::setX(int x) {
		_sprite.setPosition(x, getY());
	}

	void zombie::setY(int y) {
		_sprite.setPosition(getX(), y);
	}

	void zombie::update(void) {
		if (_moving && _movedis > 31) _moving = false;
		else if(_moving) {
			switch(_movedir) {
				case 0: setY(getY() - 1); _movedis++; break;
				case 1: setY(getY() + 1); _movedis++; break;
				case 2: setX(getX() - 1); _movedis++; break;
				case 3: setX(getX() + 1); _movedis++; break;
			}
		}

		//std::cout << getX() << " " << getY() << "\n";
	}

	extern bool colideable[];

	void zombie::input(uint16_t code) {

		int x = rand() % 4;

		if(x == 0) {
			code = input::event_up;
		} else if(x == 1) {
			code = input::event_down;
		} else if(x == 2) {
			code = input::event_left;
		} else if(x == 3) {
			code = input::event_right;
		}

		if (!_moving) {
			if (code & input::event_up) {
				_moving = true;
				_movedis = 0;
				_movedir = 0;
				if (colideable[_map[(getX() / 32) + 256 * ((getY() / 32) - 1)]])
					_moving = false;
			} else if (code & input::event_down){
				_moving = true;
				_movedis = 0;
				_movedir = 1;
				if (colideable[_map[(getX() / 32) + 256 * ((getY() / 32) + 1)]])
					_moving = false;
			} else if (code & input::event_left){
				_sprite.setScale(-1, 1);
				_sprite.setOrigin(_sprite.getLocalBounds().width, 0);
				_moving = true;
				_movedis = 0;
				_movedir = 2;
				if (colideable[_map[((getX() / 32) - 1) + 256 * (getY() / 32)]])
					_moving = false;
			} else if (code & input::event_right){
				_sprite.setScale(1, 1);
				_sprite.setOrigin(0, 0);
				_moving = true;
				_movedis = 0;
				_movedir = 3;
				if (colideable[_map[((getX() / 32) + 1) + 256 * (getY() / 32)]])
					_moving = false;
			}
		}
	}
	
	void zombie::draw(sf::RenderTarget& target, sf::RenderStates states) const {

		sf::Sprite r2 = _sprite;

		r2.setPosition(_px + getX(), _py + getY());

		target.draw(r2, states);
	}

}