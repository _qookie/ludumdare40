#pragma once

#include <engine/entity/entity.h>
#include <engine/input/input.h>

namespace entity {

	class zombie : public entity{
	public:
		zombie(std::array<int,256 * 256>& map, int x, int y, int& px, int& py);
		~zombie();
		virtual int getX(void) const override;
		virtual int getY(void) const override;
		virtual void setX(int x) override;
		virtual void setY(int y) override;
		virtual void update(void) override;
		virtual void input(uint16_t code) override;
		int _movedir;
		
	private:
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
		sf::Sprite _sprite;
		sf::Texture _tex;
		std::array<int,256 * 256>& _map;
		bool _moving;
		int _movedis;
		int& _px;
		int& _py;
		
	};

}