#pragma once

#include <string>
#include <unordered_map>
#include <memory>
#include <engine/entity/entity.h>

namespace entity {

	typedef std::unique_ptr<entity> entity_ptr;

	class entitymanager : public sf::Drawable {
	public:
		entitymanager();
		~entitymanager();
	
		void add_entity(const std::string& name, std::unique_ptr<entity> entity);
		std::unique_ptr<entity>& get_entity(const std::string& name);
		void remove_entity(const std::string& name);
		bool has_entity(const std::string& name);
		
		void update(void);
		void input(uint16_t code);

	private:
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const;
		std::unordered_map<std::string, std::unique_ptr<entity>> _entities;
		
	};

	int gethp(void);
	void inchp();

}