#pragma once

#include <engine/entity/entity.h>
#include <engine/input/input.h>

namespace entity {

	class bullet : public entity{
	public:
		bullet(std::array<int,256 * 256>& map, int dir, int& px, int& py, int x, int y);
		~bullet();
		virtual int getX(void) const override;
		virtual int getY(void) const override;
		virtual void setX(int x) override;
		virtual void setY(int y) override;
		virtual void update(void) override;
		virtual void input(uint16_t code) override;

		bool to_destroy;
		
	private:
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
		sf::Sprite _sprite;
		sf::Texture _tex;
		std::array<int,256 * 256>& _map;
		int& _px;
		int& _py;
		inline int get_tile_at(int x, int y);
		int _movedir;
	};

}