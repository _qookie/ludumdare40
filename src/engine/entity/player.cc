#include <engine/entity/player.h>
#include <iostream>
#include <cmath>

namespace entity {

	player::player(std::array<int,256 * 256>& map, int& inventory_size) :_sprite(), _map(map), _inventory_size(inventory_size) {
		_sprite.setPosition(32, 32);
		_tex.loadFromFile("res/player.png", sf::IntRect{0,0,32,32});
		_sprite.setTexture(_tex, true);
	}

	player::~player() {

	}

	int player::getX(void) const {
		return _sprite.getPosition().x;
	}

	int player::getY(void) const {
		return _sprite.getPosition().y;
	}

	void player::setX(int x) {
		_sprite.setPosition(x, getY());
	}

	void player::setY(int y) {
		_sprite.setPosition(getX(), y);
	}



	void player::update(void) {
		timer++;
		if (timer > (log(_inventory_size + 1)*10)) {
		if (_moving && _movedis > 31) _moving = false;
		else if(_moving) {
			switch(_movedir) {
				case 0: setY(getY() - 1); _movedis++; break;
				case 1: setY(getY() + 1); _movedis++; break;
				case 2: setX(getX() - 1); _movedis++; break;
				case 3: setX(getX() + 1); _movedis++; break;
			}
		}
		timer = 0;
		}
		//std::cout << getX() << " " << getY() << "\n";
	}

	bool colideable[] = {true, true, false, false, true, true, true, false, false, false, true};

	void player::input(uint16_t code) {
		if (!_moving) {
			if (code & input::event_up) {
				_moving = true;
				_movedis = 0;
				_movedir = 0;
				if (colideable[_map[(getX() / 32) + 256 * ((getY() / 32) - 1)]])
					_moving = false;
			} else if (code & input::event_down){
				_moving = true;
				_movedis = 0;
				_movedir = 1;
				if (colideable[_map[(getX() / 32) + 256 * ((getY() / 32) + 1)]])
					_moving = false;
			} else if (code & input::event_left){
				_sprite.setScale(-1, 1);
				_sprite.setOrigin(_sprite.getLocalBounds().width, 0);
				_moving = true;
				_movedis = 0;
				_movedir = 2;
				if (colideable[_map[((getX() / 32) - 1) + 256 * (getY() / 32)]])
					_moving = false;
			} else if (code & input::event_right){
				_sprite.setScale(1, 1);
				_sprite.setOrigin(0, 0);
				_moving = true;
				_movedis = 0;
				_movedir = 3;
				if (colideable[_map[((getX() / 32) + 1) + 256 * (getY() / 32)]])
					_moving = false;
			}
		}
	}
	
	void player::draw(sf::RenderTarget& target, sf::RenderStates states) const {

		sf::Sprite r2 = _sprite;

		r2.setPosition(getX() >= 427 - 16 ? 427 - 16 : getX(), getY() >= 240 - 16 ? 240 - 16 : getY());

		target.draw(r2, states);
	}

}