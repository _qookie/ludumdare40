#include <engine/entity/entitymanager.h>
#include <iostream>
#include <cstring>

namespace entity {

	entitymanager::entitymanager() {

	}

	entitymanager::~entitymanager() {

	}

	void entitymanager::add_entity(const std::string& name, std::unique_ptr<entity> entity) {
		_entities[name] = std::move(entity);
	}

	std::unique_ptr<entity>& entitymanager::get_entity(const std::string& name) {
		return _entities[name];
	}

	void entitymanager::remove_entity(const std::string& name) {
		_entities.erase(name);
	}

	int _hp = 3;

	int timer;

	int gethp() {
		return _hp;
	}

	void inchp() {
		_hp++;
	}

	void entitymanager::update(void) {


		for (auto const& x : _entities) {
			bool ch = false;
			if (has_entity("bullet") && x.first.find("zombie") == 0) {
				if (_entities["bullet"]->getX() / 32 == _entities[x.first]->getX() / 32 &&
					_entities["bullet"]->getY() / 32 == _entities[x.first]->getY() / 32) {
					remove_entity(x.first);
					remove_entity("bullet");
					
				} else {
					if (has_entity(x.first))
					x.second->update();
					
				}
				ch = true;
			} else if(x.first.find("zombie") == 0 && !ch) {
				if (has_entity(x.first)) {
					uint32_t disx = abs(_entities["player"]->getX() / 32 - _entities[x.first]->getX() / 32);
					uint32_t disy = abs(_entities["player"]->getY() / 32 - _entities[x.first]->getY() / 32);
					if (disx < 1 && disy < 1) {
						if (timer > 60) {
							_hp--;
							timer = 0;
						}

						timer++;
					} else {
						if (has_entity(x.first))
						x.second->update();
					}
				}
			} else {
					if (has_entity(x.first))
				x.second->update();
			}
		}
	}

	void entitymanager::input(uint16_t code) {
		for (auto const& x : _entities)
			x.second->input(code);
	}

	void entitymanager::draw(sf::RenderTarget& target, sf::RenderStates states) const {
		for (auto const& x : _entities)
			target.draw(*x.second, states);
	}

	bool entitymanager::has_entity(const std::string& name) {
		return _entities.find(name) != _entities.end();
	}


}