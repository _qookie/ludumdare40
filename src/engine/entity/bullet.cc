#include <engine/entity/bullet.h>
#include <iostream>

namespace entity {

	bullet::bullet(std::array<int,256 * 256>& map, int dir, int& px, int& py, int x, int y) :_sprite(), _map(map), _px(px), _py(py) {
		_sprite.setPosition(32, 32);
		_tex.loadFromFile("res/bullet.png");
		_sprite.setTexture(_tex, true);
		_sprite.setPosition(x,y);
		_movedir = dir;
		to_destroy = false;
	}

	bullet::~bullet() {

	}

	int bullet::getX(void) const {
		return _sprite.getPosition().x;
	}

	int bullet::getY(void) const {
		return _sprite.getPosition().y;
	}

	void bullet::setX(int x) {
		_sprite.setPosition(x, getY());
	}

	void bullet::setY(int y) {
		_sprite.setPosition(getX(), y);
	}

	extern bool colideable[];

	int inline bullet::get_tile_at(int x, int y) {
		return _map[x + 256 * y];
	}

	int inline get_tile_coord(int coord) {
		return coord / 32;
	}

	void bullet::update(void) {
		switch(_movedir) {
			case 0: setY(getY() - 4); break;
			case 1: setY(getY() + 4);
				if (colideable[get_tile_at(get_tile_coord(getX()), get_tile_coord(getY()) + 1)]) to_destroy = true; 
				break;

			case 2: setX(getX() - 4); break;
			case 3: setX(getX() + 4);
				if (colideable[get_tile_at(get_tile_coord(getX()) + 1, get_tile_coord(getY()))]) to_destroy = true; 
				break;


		}		

		if (colideable[get_tile_at(get_tile_coord(getX()), get_tile_coord(getY()))]) to_destroy = true; 
	}


	void bullet::input(uint16_t code) {
	}
	
	void bullet::draw(sf::RenderTarget& target, sf::RenderStates states) const {

		sf::Sprite r2 = _sprite;

		r2.setPosition(_px + getX(), _py + getY());

		target.draw(r2, states);
	}

}