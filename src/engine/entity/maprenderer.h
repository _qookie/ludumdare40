#pragma once

#include <engine/entity/entity.h>
#include <engine/input/input.h>

namespace entity {

	class maprenderer : public entity{
	public:
		maprenderer(std::array<int, 256 * 256>& map, std::array<int, 256 * 256>& items);
		~maprenderer();
		virtual int getX(void) const override;
		virtual int getY(void) const override;
		virtual void setX(int x) override;
		virtual void setY(int y) override;
		virtual void update(void) override;
		virtual void input(uint16_t code) override;
		void recreatearray();
		
	private:
		virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;
	
		sf::VertexArray _array;

		int _x, _y;

		std::array<int, 256*256>& _tiles;
		std::array<int, 256 * 256>& _items;
	};

}