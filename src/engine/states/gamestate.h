#pragma once

#include <engine/states/state.h>
#include <engine/entity/entitymanager.h>
#include <engine/entity/player.h>
#include <engine/entity/maprenderer.h>
#include <iostream>
#include <bitset>
#include <stack>

namespace states {

	struct item {
		int id;
	};

	class gamestate : public state{
	public:

		gamestate();
		~gamestate();
		
		virtual void onclose(void) override;
		virtual void update(void) override;
		virtual void render(sf::RenderWindow& window) override;
		virtual void inputevent(uint16_t code) override;
		virtual void onenter(void) override;
		virtual bool statechangeraised(void) override;
		virtual std::string changestateto(void) override;
	private:
		entity::entitymanager _entitymanager;
		std::array<int, 256*256> _tiles;
		std::array<int, 256*256> _itemsvis;
		std::array<item, 256*256> _items;
		
		sf::Texture _tilemap;
		sf::Texture _lightmask;
		sf::Texture _buttons;
		sf::Texture _itembar;
		sf::Texture _arrow_tx;
		sf::Texture _msg_bg;

		sf::Font _font;

		bool _in_msg = false;
		std::string current_string;
		int str_pos;

		sf::RenderTexture _rentex;
		
		std::vector<std::pair<sf::Vector2i, sf::Sprite>> _lights;
		
		sf::Sprite _sprite;

		sf::Sprite _itembar_sprite;

		sf::Sprite _pickup;
		sf::Sprite _attack;
		sf::Sprite _drop;
		sf::Sprite _cancel;
		sf::Sprite _use;
		sf::Sprite _pause;
		sf::Sprite _items_spr;
		sf::Sprite _arrow;
		sf::Sprite _message;

		std::vector<item> _inv;
		sf::Sprite _inv_sprites[10];

		sf::Sprite _health_sprts[3];

		bool _in_inv;
		std::stack<std::string> _messages;

		void loadlevel(std::string img_path, std::string dat_path);
	
		std::string getnextmessage(void);
	};
}