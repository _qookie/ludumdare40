#include <engine/states/gamestate.h>
#include <memory>
#include <engine/utils/datparser.h>
#include <engine/input/input.h>
#include <engine/entity/bullet.h>
#include <engine/entity/zombie.h>
#include <string>
#include <cstdlib>

namespace states {

	sf::Texture tx;

	int _hp, shit;

	gamestate::gamestate() {
		_tilemap.loadFromFile("res/tilemap.png");
		_lightmask.loadFromFile("res/lightmask.png");
		_itembar.loadFromFile("res/itembar.png");
		_buttons.loadFromFile("res/buttons.png");
		_arrow_tx.loadFromFile("res/arrow.png");
		_msg_bg.loadFromFile("res/message.png");
		_entitymanager.add_entity("player", entity::entity_ptr(new entity::player(_tiles, shit)));
		_entitymanager.add_entity("maprenderer", entity::entity_ptr(new entity::maprenderer(_tiles, _itemsvis)));
		loadlevel("res/level1.png", "res/level1.dat");

		((entity::maprenderer*)(_entitymanager.get_entity("maprenderer").get()))->recreatearray();

		_entitymanager.get_entity("maprenderer")->setX(0);		
		_entitymanager.get_entity("maprenderer")->setY(0);

		_sprite.setTexture(_lightmask, true);
		_sprite.setOrigin(_lightmask.getSize().x / 2, _lightmask.getSize().y / 2);
		_sprite.setScale(0.75f, 0.75f);

		_rentex.create(854, 480, false);

		_itembar_sprite.setTexture(_itembar, true);
		_itembar_sprite.setPosition(854 - 40, 240 - 182);
		
		_pickup.setTexture(_buttons, true);
		_pickup.setTextureRect(sf::IntRect{0,0,172,28});
		_attack.setTexture(_buttons, true);
		_attack.setTextureRect(sf::IntRect{0, 28, 172, 28});
		_drop.setTexture(_buttons, true);
		_drop.setTextureRect(sf::IntRect{0, 56, 172, 28});
		_cancel.setTexture(_buttons, true);
		_cancel.setTextureRect(sf::IntRect{0, 84, 172, 28});
		_use.setTexture(_buttons, true);
		_use.setTextureRect(sf::IntRect{0, 111, 172, 28});
		_pause.setTexture(_buttons, true);
		_pause.setTextureRect(sf::IntRect{0, 139, 172, 28});
		_items_spr.setTexture(_buttons, true);
		_items_spr.setTextureRect(sf::IntRect{0, 166, 172, 28});

		tx.loadFromFile("res/player.png");

		_health_sprts[0].setTexture(tx, true);
		_health_sprts[1].setTexture(tx, true);
		_health_sprts[2].setTexture(tx, true);
		_health_sprts[0].setPosition(854 - 32 * 3, 0);
		_health_sprts[1].setPosition(854 - 32 * 2, 0);
		_health_sprts[2].setPosition(854 - 32 * 1, 0);
		

		_arrow.setTexture(_arrow_tx, false);
		
		_font.loadFromFile("res/font.ttf");

		_message.setTexture(_msg_bg, true);
		_message.setPosition(427 - 273, 480 - 117);
	

		_messages.push("Oh, you woke up! I better explain what happen. You have been taken to this dungeon.\n\
You seem like you don't remember anything, so I'll help you a little.\n\
You can use arrow keys or the joystick to move.\nActions you can take are described at the bottom of the screen.\n");
	}

	int _inv_idx;

	int px;
	int py;

	void gamestate::loadlevel(std::string img_path, std::string dat_path) {
		sf::Texture tex;
		tex.loadFromFile(img_path);
		sf::Image pixels = tex.copyToImage();
		utils::container contaiter = utils::loadcontainerfromfile(dat_path);
		int item_i = 0;
		for (int x = 0; x < 256; x++) {
			for (int y = 0; y < 256; y++) {
				sf::Color pixel = pixels.getPixel(x, y);
				
				_tiles[x + 256 * y] = 4;
				if(pixel == sf::Color(0x62, 0x62, 0x62)) _tiles[x + 256 * y] = 1;
				if(pixel == sf::Color(0x20, 0x20, 0x20)) _tiles[x + 256 * y] = 2;
				if(pixel == sf::Color(0x5c, 0x36, 0x0a)) _tiles[x + 256 * y] = 3;
				if(pixel == sf::Color(0x46, 0x20, 0x20)) {
					_tiles[x + 256 * y] = _tiles[(x - 1) + 256 * y] == 2 ? 5 : 6;
					_lights.push_back(std::make_pair(sf::Vector2i(x * 32 + 16, y * 32 + 16), sf::Sprite(_lightmask)));
					_lights.back().second.setOrigin(_lightmask.getSize().x / 2, _lightmask.getSize().y / 2);			
				}
				if(pixel == sf::Color(0x82, 0x12, 0x12)) {
					_tiles[x + 256 * y] = _tiles[(x - 1) + 256 * y];
					_itemsvis[x + 256 * y] = utils::parse_num(contaiter.data["item" + std::to_string(x) + "-" + std::to_string(y) + "_id"]);
					_items[x + 256 * y] = item{utils::parse_num(contaiter.data["item" + std::to_string(x) + "-" + std::to_string(y) + "_id"])};
					item_i++;
					

				}
				if(pixel == sf::Color(0x40, 0x40, 0x40)) _tiles[x + 256 * y] = 10;
				

				if(pixel == sf::Color(0x19, 0x41, 0x08)) {
					_tiles[x + 256 * y] = _tiles[(x - 1) + 256 * y];
					px = _entitymanager.get_entity("maprenderer")->getX();
					py = _entitymanager.get_entity("maprenderer")->getY();
					_entitymanager.add_entity("zombie" + std::to_string(x) + "-" + std::to_string(y), entity::entity_ptr(new entity::zombie(_tiles, x * 32, y * 32, px, py)));
				}

				if(pixel == sf::Color(0x21, 0x44, 0xc4)) _tiles[x + 256 * y] = 11;
				 
			}
		}

		shit = _inv.size();

	}

	gamestate::~gamestate() {

	}
	
	void gamestate::onclose(void) {

	}

	int inline screenx(int x) {
		if(x >= 427) return 427;
		else return x;
	}

	int inline screeny(int y) {
		if(y >= 240) return 240;
		else return y;
	}

	bool albe_to_pick_up = false;

	void gamestate::update(void) {
		px = _entitymanager.get_entity("maprenderer")->getX();
		py = _entitymanager.get_entity("maprenderer")->getY();
				

		_entitymanager.update();

		if(_entitymanager.has_entity("bullet")) {
			if (((entity::bullet*)(_entitymanager.get_entity("bullet").get()))->to_destroy) {
				_entitymanager.remove_entity("bullet");
			}
		}

		if (_entitymanager.get_entity("player")->getX() >= 427 - 16) {
			_entitymanager.get_entity("maprenderer")->setX(-(_entitymanager.get_entity("player")->getX() - 427 + 16));		
		}

		if (_entitymanager.get_entity("player")->getY() >= 240 - 16) {
			_entitymanager.get_entity("maprenderer")->setY(-(_entitymanager.get_entity("player")->getY() - 240 + 16));		
		}

		((entity::maprenderer*)(_entitymanager.get_entity("maprenderer").get()))->recreatearray();

		_sprite.setPosition(
        	screenx(_entitymanager.get_entity("player")->getX() + 16),
        	screeny(_entitymanager.get_entity("player")->getY() + 16));

		if (_itemsvis[(_entitymanager.get_entity("player")->getX() / 32) + 256 * (_entitymanager.get_entity("player")->getY() / 32)] > 0) {
			albe_to_pick_up = true;
		} else {
			albe_to_pick_up = false;
		}

		if (!_in_msg && current_string.size() < 1 && _messages.size() > 0) {
			current_string = _messages.top();
			_messages.pop();
			str_pos = 0;
			_in_msg = true;
		}

		_hp = entity::gethp();

		if (_hp == 0) _messages.push("You died.");
		if (_tiles[_entitymanager.get_entity("player")->getX() / 32 + 256 * _entitymanager.get_entity("player")->getY() / 32] == 11) {
			_messages.push("You won!");
		}
		
	}

	std::string gamestate::getnextmessage(void) {
		std::string message = _messages.top();
		_messages.pop();
		str_pos = 0;
		return message; //TODO: parse data messages(getting coins etc)
	}


	void gamestate::render(sf::RenderWindow& window) {
        window.clear(sf::Color(0,0,0));
        _rentex.clear(sf::Color(0,0,0));
        sf::RenderStates states;

        states.texture = &_tilemap;
        
        states.blendMode = sf::BlendAdd;

        _rentex.draw(_sprite, states);

        for (auto& i : _lights) {

        	i.second.setPosition(_entitymanager.get_entity("maprenderer")->getX() + i.first.x, _entitymanager.get_entity("maprenderer")->getY() + i.first.y);

        	if (!((i.second.getPosition().x < -128 || i.second.getPosition().x > 854+128) &&
        		(i.second.getPosition().y < -128 || i.second.getPosition().y > 240+128) ))
        		_rentex.draw(i.second, states);
        }

        _rentex.display();
        
        states.blendMode = sf::BlendAdd;        
		//window.draw(*_entitymanager.get_entity("maprenderer"), states);
        //window.draw(*_entitymanager.get_entity("player"), states);
        //if (_entitymanager.has_entity("bullet")) {
       // 	window.draw(*_entitymanager.get_entity("bullet"), states);
        //}

        window.draw(_entitymanager, states);
        
        states.blendMode = sf::BlendMultiply;
        window.draw(sf::Sprite(_rentex.getTexture()), states);
        states.blendMode = sf::BlendAdd;
        if(_in_inv)
		window.draw(_itembar_sprite, states);

        _pickup.setPosition(0, 452);
	    _attack.setPosition(0, 452);
	    _drop.setPosition(360, 452);
	    _use.setPosition(0, 452);
	    _pause.setPosition(180, 452);
	    _cancel.setPosition(180, 452);
	    _items_spr.setPosition(361, 452);


	    if (!_in_msg) {
		    if(_in_inv) {
				window.draw(_use);
		    } else {
			    if (albe_to_pick_up) window.draw(_pickup);
			    else window.draw(_attack);
		    }
		    
		    window.draw(_in_inv ? _cancel : _pause);
		    
		    window.draw(_in_inv ? _drop : _items_spr);
		}

	    if (_in_inv) {
	    	_arrow.setPosition(810 - 32, 240 - 182 + 4 + 36 * _inv_idx);
	    	window.draw(_arrow);
	    }

	    if (_in_inv) {

	    int _i = 0;
	    for(item i : _inv) {

	    	if (i.id > 0) {
		    	int uu = 32 * (i.id % 8);
	            int vv = 32 * (i.id / 8);

		    	_inv_sprites[_i].setTexture(_tilemap);
		    	_inv_sprites[_i].setTextureRect(sf::IntRect{uu, vv, 32, 32});
		    	_inv_sprites[_i].setPosition(854 - 36, 240 - 182 + 4 + 36 * _i);
		    	window.draw(_inv_sprites[_i], states);
			}

	    	_i++;
	    }
		}

	    if (_in_msg) {
		    window.draw(_message, states);
		    
		    sf::Text text;
		    text.setFont(_font);
		    text.setString(current_string.substr(0, str_pos));
		    text.setCharacterSize(16);
			text.setColor(sf::Color::Black);
			text.setPosition(427 - 546 / 2 + 8, 240 + 125);

			window.draw(text);
			str_pos++;
		}

		if (_hp > 2)
			window.draw(_health_sprts[2], states); 
		if (_hp > 1)
			window.draw(_health_sprts[1], states);
		if (_hp > 0)
			window.draw(_health_sprts[0], states);

	}

	void gamestate::onenter(void) {
		
	}
	
	bool previous_b = false;
	bool previous_y = false;
	bool previous_a = false;
	bool previous_up = false;
	bool previous_down = false;

	void gamestate::inputevent(uint16_t code) {


		if((code & input::event_a || code & input::event_b || code & input::event_y) && _in_msg) {
			if (current_string.size() < str_pos) {
				// get next or get out
				if (_messages.size() > 0) {
					current_string = getnextmessage();
					str_pos = 0;
				} else {
					_in_msg = false;
					current_string = "";
					if (_hp == 0) std::exit(0);
					if (_tiles[_entitymanager.get_entity("player")->getX() / 32 + 256 * _entitymanager.get_entity("player")->getY() / 32] == 11) std::exit(0);
				}

				previous_a = code & input::event_a;
				previous_b = code & input::event_b;
				previous_y = code & input::event_y;
			}

		} else if(!_in_msg) {

			// pick up

			bool xd = false;

			if (code & input::event_b && albe_to_pick_up && !previous_b && !_in_inv && _inv.size() < 10) {
				_inv.push_back(_items[(_entitymanager.get_entity("player")->getX() / 32) + 256 * (_entitymanager.get_entity("player")->getY() / 32)]);
				_itemsvis[(_entitymanager.get_entity("player")->getX() / 32) + 256 * (_entitymanager.get_entity("player")->getY() / 32)] = 0;
				_items[(_entitymanager.get_entity("player")->getX() / 32) + 256 * (_entitymanager.get_entity("player")->getY() / 32)].id = 0;
				

			} else if (code & input::event_b && !albe_to_pick_up && !previous_b && !_in_inv) {
				// attack
				px = _entitymanager.get_entity("maprenderer")->getX();
				py = _entitymanager.get_entity("maprenderer")->getY();
				int ppx = _entitymanager.get_entity("player")->getX();
				int ppy = _entitymanager.get_entity("player")->getY();
				if(!_entitymanager.has_entity("bullet")) 
					_entitymanager.add_entity("bullet", entity::entity_ptr(new entity::bullet(_tiles,
					(((entity::player*)(_entitymanager.get_entity("player").get()))->_movedir),
					px, py, ppx, ppy)));
				xd = true;
				
			} else if(code & input::event_b && _in_inv && !previous_b) {
				// use
					
				if (_inv_idx < _inv.size()) {
					int ppx = _entitymanager.get_entity("player")->getX() / 32;
					int ppy = _entitymanager.get_entity("player")->getY() / 32;
						
					switch(_inv[_inv_idx].id) {
						case 8: 
							if (_tiles[ppx + 256 * (ppy - 1)] == 10) {
								_messages.push("A door has been opened.");
								_inv.erase(_inv.begin() + _inv_idx);
								_tiles[ppx + 256 * (ppy - 1)] = 2;
							} else if (_tiles[ppx + 256 * (ppy + 1)] == 10) {
								_messages.push("A door has been opened.");
								_inv.erase(_inv.begin() + _inv_idx);
								_tiles[ppx + 256 * (ppy + 1)] = 2;
							} else if (_tiles[ppx - 1 + 256 * ppy] == 10) {
								_messages.push("A door has been opened.");
								_inv.erase(_inv.begin() + _inv_idx);
								_tiles[ppx - 1 + 256 * ppy] = 2;
							} else if (_tiles[ppx + 1 + 256 * ppy] == 10) {
								_messages.push("A door has been opened.");
								_inv.erase(_inv.begin() + _inv_idx);
								_tiles[ppx + 1 + 256 * ppy] = 2;
							} else

							_messages.push("A small key, use it next to a door to open it.");
							break;

						case 9:
							if (_hp < 3) {
								entity::inchp();
								_messages.push("Restored 1 health point.");
								_inv.erase(_inv.begin() + _inv_idx);
							} else {
								_messages.push("A medkit, can restore 1 health point.");
							}
					}
				}
				previous_b = code & input::event_b;


			}
			previous_b = code & input::event_b;

	 

			
			// inventory | drop
			if (code & input::event_y && !_in_inv && !previous_y) {
				_in_inv = true;
			} else if (code & input::event_y && _in_inv && !previous_y && _inv.size() > 0) {
				if (_inv_idx < _inv.size()) { 
					_itemsvis[(_entitymanager.get_entity("player")->getX() / 32) + 256 * (_entitymanager.get_entity("player")->getY() / 32)] = _inv[_inv_idx].id;
					_items[(_entitymanager.get_entity("player")->getX() / 32) + 256 * (_entitymanager.get_entity("player")->getY() / 32)] = _inv[_inv_idx];
					_inv.erase(_inv.begin() + _inv_idx);
					_in_inv = false;
				}
			}

			previous_y = code & input::event_y;

			if (code & input::event_a && _in_inv && !previous_a) {
				_in_inv = false;
			}

			previous_a = code & input::event_a;

			if (code & input::event_up && _in_inv && !previous_up) {
				_inv_idx --;
			}

			if (_inv_idx < 0) _inv_idx = 9;

			previous_up = code & input::event_up;

			if (code & input::event_down && _in_inv && !previous_down) {
				_inv_idx ++;
			}

			if (_inv_idx > 9) _inv_idx = 0;

			previous_down = code & input::event_down;


			if (!_in_inv)
				_entitymanager.input(code);
			else {

			}
		}
	}


	bool gamestate::statechangeraised(void) {
		return false;
	}

	std::string gamestate::changestateto(void) {
		return "";
	}

}