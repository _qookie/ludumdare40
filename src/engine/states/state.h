#pragma once

#include <SFML/Graphics.hpp>
#include <string>

namespace states {

	class state {
	public:
		
		virtual void onclose(void) = 0;
		virtual void update(void) = 0;
		virtual void render(sf::RenderWindow& window) = 0;
		virtual void inputevent(uint16_t code) = 0;
		virtual void onenter(void) = 0;
		virtual bool statechangeraised(void) = 0;
		virtual std::string changestateto(void) = 0;

	};

}