#include <engine/states/statemanager.h>

namespace states {

	statemanager::statemanager() {

	}

	statemanager::~statemanager() {

	}

	void statemanager::add_state(const std::string& name, std::unique_ptr<state> state) {
		_states[name] = std::move(state);
	}

	std::unique_ptr<state>& statemanager::get_state(const std::string& name) {
		return _states[name];
	}

	void statemanager::enter_state(const std::string& name) {
		if (_current_state.size() > 0)
			_states[_current_state]->onclose();

		_current_state = name;

		_states[_current_state]->onenter();
	}

	std::unique_ptr<state>& statemanager::get_current_state(void) {
		return _states[_current_state];
	}	

}