#pragma once

#include <string>
#include <unordered_map>
#include <memory>
#include <engine/states/state.h>

namespace states {

	typedef std::unique_ptr<states::state> state_ptr;

	class statemanager {
	public:
		statemanager();
		~statemanager();
	
		void add_state(const std::string& name, std::unique_ptr<state> state);
		std::unique_ptr<state>& get_state(const std::string& name);
		void enter_state(const std::string& name);
		std::unique_ptr<state>& get_current_state(void);
		
	private:

		std::unordered_map<std::string, std::unique_ptr<state>> _states;
		std::string _current_state;

	};

}