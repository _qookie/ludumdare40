#pragma once

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <cassert>
#include <chrono>
#include <engine/states/statemanager.h>
#include <engine/input/inputmanager.h>
#include <engine/states/gamestate.h>
#include <memory>

namespace engine {

	class game {
	public:
		game();
		~game();
	
		void run();

	private:

		sf::RenderWindow _window;
		states::statemanager _statemanager;
		input::inputmanager _inputmanager;

		void close(void);
		void render(void);
		void update(void);
		void pollEvents(sf::Event& ev);

	};

}