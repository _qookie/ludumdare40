#include <engine/input/inputmanager.h>

namespace input {

	inputmanager::inputmanager(states::statemanager& statemanager) :_statemanager(statemanager) {

	}
	
	inputmanager::~inputmanager() {

	}

	void inputmanager::update(void) {

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) || sf::Joystick::getAxisPosition(0, sf::Joystick::Y) < -50) {
			_currenteventcode |= event_up;	
		} else {
			_currenteventcode &= ~event_up;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) || sf::Joystick::getAxisPosition(0, sf::Joystick::Y) > 50) {
			_currenteventcode |= event_down;	
		} else {
			_currenteventcode &= ~event_down;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) || sf::Joystick::getAxisPosition(0, sf::Joystick::X) < -50) {
			_currenteventcode |= event_left;
		} else {
			_currenteventcode &= ~event_left;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) || sf::Joystick::getAxisPosition(0, sf::Joystick::X) > 50) {
			_currenteventcode |= event_right;	
		} else {
			_currenteventcode &= ~event_right;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::A) || sf::Joystick::isButtonPressed(0, 3)) {
			_currenteventcode |= event_y;
		} else {
			_currenteventcode &= ~event_y;
		}
		
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::S) || sf::Joystick::isButtonPressed(0, 0)) {
			_currenteventcode |= event_x;
		} else {
			_currenteventcode &= ~event_x;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Z) || sf::Joystick::isButtonPressed(0, 2)) {
			_currenteventcode |= event_b;
		} else {
			_currenteventcode &= ~event_b;
		}
		
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::X) || sf::Joystick::isButtonPressed(0, 1)) {
			_currenteventcode |= event_a;
		} else {
			_currenteventcode &= ~event_a;
		}
		
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Return) || sf::Joystick::isButtonPressed(0, 9)) {
			_currenteventcode |= event_start;
		} else {
			_currenteventcode &= ~event_start;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::RShift) || sf::Joystick::isButtonPressed(0, 8)) {
			_currenteventcode |= event_select;
		} else {
			_currenteventcode &= ~event_select;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q) || sf::Joystick::isButtonPressed(0, 4)) {
			_currenteventcode |= event_lb;
		} else {
			_currenteventcode &= ~event_lb;
		}

		if (sf::Keyboard::isKeyPressed(sf::Keyboard::W) || sf::Joystick::isButtonPressed(0, 5)) {
			_currenteventcode |= event_rb;
		} else {
			_currenteventcode &= ~event_rb;
		}

		_currenteventcode &= 0b0000111111111111;
	
		_statemanager.get_current_state()->inputevent(_currenteventcode);
	}

}