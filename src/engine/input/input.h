#pragma once

#include <stdint.h>

namespace input {

	constexpr uint16_t event_up 		= 0b0000000000000001;
	constexpr uint16_t event_down 		= 0b0000000000000010;
	constexpr uint16_t event_left		= 0b0000000000000100;
	constexpr uint16_t event_right		= 0b0000000000001000;
	constexpr uint16_t event_start		= 0b0000000000010000;
	constexpr uint16_t event_select		= 0b0000000000100000;
	constexpr uint16_t event_a 			= 0b0000000001000000;
	constexpr uint16_t event_b			= 0b0000000010000000;
	constexpr uint16_t event_x			= 0b0000000100000000;
	constexpr uint16_t event_y			= 0b0000001000000000;
	constexpr uint16_t event_lb			= 0b0000010000000000;
	constexpr uint16_t event_rb			= 0b0000100000000000;
	

}