#pragma once

#include <engine/states/statemanager.h>
#include <SFML/Window.hpp>
#include <engine/input/input.h>
#include <iostream>

namespace input {

	class inputmanager {
	public:
		inputmanager(states::statemanager&);
		~inputmanager();
		
		void update(void);

	private:
		states::statemanager& _statemanager;
		uint16_t _currenteventcode;

	};

}